///Bryan Martinez_2/2/18_CSE02
//This program will give me specific information based on two data sets.
public class Cyclometer
{
  // main method required for every java program
  public static void main (String[] args) 
  {
    int secsTrip1=480; // The seconds it took for the first trip.
    int secsTrip2=3220; // The seconds it took for the second trip.
    int countsTrip1=1561; // The amount of rotations the first trip did.
    int countsTrip2=9037; // THe amount of rotations the second trip did. 
    double wheelDiameter=27.0, // This is the diameter of the wheel.
      PI=3.14159, // This is the number for pi.
    feetPerMile=5280, // This is how many feet are in each mile.
    inchesPerFoot= 12, // This is how many inches are in each feet. 
    secondsPerMinute=60; // This is how many seconds are in a minute.
    double distanceTrip1, distanceTrip2, totalDistance; //
    System.out.println("Trip 1 took " +
                       (secsTrip1/secondsPerMinute) + " minutes and had " +
                       countsTrip1 +" counts."); 
    System.out.println("Trip 2 took " +
                       (secsTrip2/secondsPerMinute) +" minutes and had "+
                       countsTrip2+" counts.");
    //
    //
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //Above give distance in inches.
    //
    //
    distanceTrip1/= inchesPerFoot* feetPerMile; //
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance= distanceTrip1+distanceTrip2;
      
    System.out.println("Trip 1 was"+ distanceTrip1+ "miles");
    System.out.println( "Trip 2 was"+ distanceTrip2+ "miles");
    System.out.println("The total distance was"+ totalDistance+ "miles");
  } //end of main method
} //end of class