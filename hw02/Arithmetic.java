// Bryan Martinez CSE 02 

public class Arithmetic 
{
  //main method required for every java program. 
  public static void main (String [] args)
  {
    // Number of pair of pants
    int numPants= 3;
    // Cost per pair of pants.
    double pantsPrice= 34.98;
    // Number of sweatshirts.
    int numShirts= 2;
    // Cost per shirt.
    double shirtPrice= 24.99;
    //Number of belts.
    int numBelts= 1;
    //Cost per belt.
    double beltCost= 33.99;
    //Tax rate.
    double paSalesTax= 0.06;
    
    double totalCostOfPants= pantsPrice*numPants; 
    double totalCostOfShirts= shirtPrice*numShirts;
    double totalCostOfBelts= beltCost*numBelts;
    
    double taxOnPants = totalCostOfPants*paSalesTax;
    double taxOnBelts= totalCostOfBelts*paSalesTax;
    double taxOnShirts= totalCostOfShirts*paSalesTax;
    
    double totalCostOfShirtsWithTax= totalCostOfShirts+ taxOnShirts;
    double totalCostOfBeltsWithTax= totalCostOfBelts+ taxOnBelts;
    double totalCostOfPantsWithTax= totalCostOfPants+ taxOnPants;
    
    double totalCostOfPurchaseBeofreTax= totalCostOfPants+totalCostOfBelts+totalCostOfShirts;
    
    double totalCostOfPurchaseWithTax= totalCostOfPantsWithTax+totalCostOfBeltsWithTax+totalCostOfShirtsWithTax;
  
  System.out.println("TotalPriceOfShirts" + totalCostOfShirtsWithTax);
  System.out.println("TotalPriceOfPants"+ totalCostOfPantsWithTax);
  System.out.println("TotalPriceOfBelts"+ totalCostOfBeltsWithTax);
  System.out.println("TotalPriceOfPurchase"+ totalCostOfPurchaseWithTax);
  }
}