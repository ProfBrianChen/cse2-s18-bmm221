Grade:     90/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
Yes
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Incorrect amount of decimal places in output.
C) How can any runtime errors be resolved?
Multiply by 100, convert to int, divide result by 100.
D) What topics should the student study in order to avoid the errors they made in this homework?
Reread hint in homework assignment.
E) Other comments:
Also missing output of total price and tax for each item.
