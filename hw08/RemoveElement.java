//Bryan Martinez_CSE2_April 9
//Program runs on the basis of another array. 

import java.util.Scanner;
public class RemoveElement
{
  public static void main(String[]args)
  {
    Scanner scan = new Scanner(System.in);
    int numb[] = new int [10];
    
    int newArray1[];
    int newArray2[];
    int index, target;
    String ans = " ";
    
    do {
      System.out.print ("Input ten random integers from 0 to 9 : ");
      
      numb = randomInput();
      String out = "The first array is:";
      
      out += listArray(numb);
      System.out.println(out);
      
      
      System.out.println ("Input index:");
      
      index = scan.nextInt();
      
      newArray1= delete (numb,index);
      String out1= "The output is:";
      out1 +=listArray(newArray1);
      System.out.println(out1);
      
      
      System.out.print(" GO Again. Input 'y' or 'Y', anything else to quit");
      ans = scan.next();
        
        
 }
    while (ans.equals("y")|| ans.equals("Y"));
  }
  
  public static String listArray(int numb []){
    String out = "{";
    
    for (int b = 0; b<numb.length; b++)
    {
      if(b>0){
        out+=", ";
      }
      out+=numb[b];
    }
    out+=" } ";
    return out;
  }
  
  public static int [] randomInput()
  {
    int[] random = new int [10];
    for (int i = 0; i<10; i++){
      int randomVal= (int)((Math.random()) * 10);
      System.out.println(randomVal);
      random[i]= randomVal;
    }
    return random;
  }
  
  public static int[] delete (int [] list, int poss)
  {
    list = new int[10];
    
    for (int i=0; i<10; i++)
    {
      if (i == poss)
      {
        System.out.println(list[i-poss]);
      }
    }
    return list;
  }
  
  public static int [] remove (int [] list, int target)
  {
    list = new int [10];
    
    for (int i = 0; i<10; i++)
    {
      if (i == target) {
        System.out.println(list[i-target]);
      }
    }
    return list;
  }
}