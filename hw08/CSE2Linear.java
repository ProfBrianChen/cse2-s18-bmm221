//Bryan Martinez_Cse02_hw08
//Array is meant to scramble user input data.

import java.util.Random;
import java.util.Scanner;

public class CSE2Linear // Beginning of class
{
  public static void main(String [] args)// main method used for every java program. 
  {
    Scanner myScanner = new Scanner(System.in);// New scanner.
    
    int [] grades =new int[15]; //Values to array
    
    System.out.println("Enter 15 integers for final grades:");
    
    int firstIntput= 0;
    int secIntput=0;
    
    for (int i = 0; i< grades.length; i++)
    {
      while (true)
      {
        if (myScanner.hasNextInt())
        {
          firstIntput= myScanner.nextInt();
          if (firstIntput <= 100 && firstIntput >= 0)
          {
            if(firstIntput>=secIntput)
            {
              grades[i]= firstIntput;
              secIntput=firstIntput;
              
              break;}
              
              else {
                System.out.println("Error: Enter correct values:");
              }
          }
            
            
            else {
              System.out.println ("Error: Enter values in range. ");
            }
          }
        
          else {
            myScanner.next();
            System.out.println ("Error: Enter integer.");
          }
        
      }
        
    
  }
  

    for(int i=0; i<grades.length; i++)
    {
      System.out.print(grades[i] + "n");
    }
    System.out.println("Input a grade.");
    int target = myScanner.nextInt();
    int numbSearch= binarySearch(target, grades);
    
    
    if (numbSearch >= 0)
    {
      System.out.println(target + " was found"+ numbSearch + "iterations");
    }
    else {
      System.out.println(target + "was not found" + -numbSearch + "iterations");
    }
    
    
    
    int[] newArray;
    newArray= scrambledArray(grades);
    
    for (int i=0; i<newArray.length; i++){
      System.out.print(newArray[i]+ " ");
    }
    
    System.out.println("Enter a grade to find: ");
    
    int rsearch= myScanner.nextInt();
    int linSearch= linearSearch(grades, 15, rsearch);
    
    if(linSearch>0)
    {
      System.out.println(rsearch + "was found" + linSearch+ "iterations");
    }
    
    else {
      System.out.println(rsearch+ "was  not found"+ -linSearch+ "iterations");
      
    }
  }
  
  public static int binarySearch (int data, int[] array)
  {
    int low= 0;
    int middle = 0;
    int high= array.length-1;
    int count = 0; 
    
    while (high >= low)
    {
      count++;
      middle = (high + low)/2;
      
      if (array[middle]==data){
        return count;
      }
      if (array[middle] < data){
        low = middle + 1;
      }
      if (array[middle]> data){
        high = middle -1;
      }
    }
    return -count;
    }
  public static int[] scrambledArray(int[] array){
    int x = array.length;
    int y= 0;
    int z=0;
    Random random = new Random ();
    random.nextInt();
    
    
    for (int i=0; i<x; i++){
      z = random.nextInt(x);
      y = array[z];
      array[z]= array[i];
      array[i]= y;
    }
    return array;
        
        
    }
  public static int linearSearch(int array[],  int r, int x)
  {
    int counter=0;
    
    for(int i=0; i<r; i++)
    {
      counter++;
      
      if (array[i]== x)
      {
        return counter;
      }
    }
    return -counter;
  }
  }// end of class


  

  
 
  