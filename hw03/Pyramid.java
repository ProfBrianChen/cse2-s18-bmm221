//Bryan Martinez_CSE02
import java.util.Scanner;

public class Pyramid 
{
  public static void main(String[]args) // main method for every Java program
  {
    Scanner myScanner = new Scanner (System.in); //calling a new scanner
    
    System.out.println("Enter side of the pyramid");
    
    double rLength= myScanner.nextDouble();
    
    System.out.println("Enter height");
    
    double hLength= myScanner.nextDouble();
    
    double totVOL;
    
    totVOL = ((rLength*rLength*hLength)/3);
    
    System.out.println(totVOL+ "is the volume");
  }//main method ends
} //class ends