//Bryan Martinez --CSE02-- 3/26/18
// This program is meant to output the area of any shape a user inputs.This

import java.util.Scanner ;
public class Area 
{
  //main method required for every java program. 
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
    
    String newShape =shape(myScanner);
    if (newShape.equals ("rectangle"))
    {
      System.out.println("The area of the reactangle is " + rectangle(myScanner));
      
    }
    
      else if (newShape.equals ("circle"))
      {
        System.out.println ("The area of the circle is " + circle(myScanner));
        
      }
    
    else if (newShape.equals("triangle")){
      System.out.println("The area of the triangle is" + triangle(myScanner));
    }
    
  }//end of else if statment
  
// end of main method.

public static String shape(Scanner s) 
  
{
  System.out.println ("Type 'circle', 'triangle', or 'rectangle'");
  String shape = s.next ();
  while (!shape.equals ("triangle") && !shape.equals ("rectangle") && !shape.equals ("circle") )
  {
    System.out.println ("Error: Enter a valid shape");
    shape = s.next();
    
  }
  return shape;
}// end of method

public static double rectangle(Scanner s)
{
  System.out.println("Enter length of rectangle");
  double length ;
  while (true) {
    if (s.hasNextDouble())
    {
      length= s.nextDouble();
      break;
      
    }
    else {
      s.next();
      System.out.println("Error: Enter correct variable");
    }
  }
  System.out.println("Enter height for rectangle");
  double height ;
  while (true)
  {
    if (s.hasNextDouble ()) 
    {
      height = s.nextDouble();
      break;
      
    }
    else {
      s.next();
      System.out.println("Error; Please enter a height");
      
    }
  }
  return height * length;
}

public static double triangle(Scanner s)
{
  System.out.println("Input base length of the triangle");
  double base;
  while (true)
  {
    if(s. hasNextDouble())
    {
      base =s.nextDouble();
      break;
    }
    else {
      s.next (); 
      System.out.println("Error: Enter a base length");
    }
     }
  System.out.println("Input height of triangle");
  double height;
  while (true)
  {
    if (s.hasNextDouble ())
    {
      height=s.nextDouble ();
      break;
    }
    else{
      s.next();
      System.out.println ("Error:enter an acceptable height");
    }
  }
  return (base*height)/2;
  
}

public static double circle(Scanner s)
{
  System.out.println("Enter radius of circle");
  double radius ;
  while (true){
    if (s.hasNextDouble())
    {
      radius = s.nextDouble();
      break;
    }
    else {
      s.next();
      System.out.println ("Error: input a correct value");
    }
    
  }
  return Math.PI*Math.pow(radius,2);
  
}
}