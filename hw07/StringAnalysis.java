//Bryan Martinez_CSE02_3/27/18
//This code is meant to run strings and distingush if the characters are letters.

import java.util.Scanner;
public class StringAnalysis {
  //main method required for all java programs.
  public static void main(String[]args)
  {
   //Declaring Scanner 
    Scanner myScanner = new Scanner (System.in);
    boolean string = true;
    boolean examine = true;
    String answer = "";
    int val = 0;
    
    while (string){
      System.out.print("Enter a string");
      if (!myScanner.hasNextInt() && !myScanner.hasNextFloat() && !myScanner.hasNextDouble())
      {
        string =false;
      }
      else {
        myScanner.next();
        
      }//end of statement
    }//end of loop
  answer= myScanner.next();
  
  
  while (examine)
  {
    System.out.println("Enter '0' if you want to examine characters.");
    val =myScanner.nextInt();
    if (val >=0)
    {
      examine =false;
      break;
      
    }
    else {
      myScanner.next();
      System.out.println("Error; Enter correct value");
      examine = true;
    }//end of else
  }//end of loop
  
  if (val ==0)
  {
    if(string (answer))
    {
      System.out.print("All examined characters were letters.");
    }
    else{
      System.out.print("Not all characters are letter.");
    }
  }
  if (val>0)
  {
    if(string(answer,val)){
      System.out.println("All characters were letters.");
    }
    else{
      System.out.println("Not all characters were letters.");
    }
  }
  }


public static boolean string (String enteredWord) 
{
  for (int i = 0; i<enteredWord.length(); i++)
  {
    if (!Character.isLetter(enteredWord.charAt(i)))
    {
      return false;
    }
  }
  return true;
}

public static boolean string (String enteredWord, int number)
{
  if (number>enteredWord.length ())
  {
    number= enteredWord.length();
  }
  for (int i=0; i<number; i++)
  {
    if(!Character.isLetter(enteredWord.charAt(i)))
    {
      return false;
    }
    }
  return true;
  }
}
